# IgbuduEyeTrackingAnalysis

Source code of Mary Igbudu's master thesis and research paper

## Description
The intention of this upload is to share the code that was used to analyze eye-tracking data from a Varjo VR-2 headset.

## Usage
Record eye-tracking data through Varjo Base software. In addition, enable screen recording to store a video of the users' VR usage.
Afterwards, in an offline process, convert the recorded user video to single images (e.g. using Irfanview). Then, use the files provided here to calculate the the gaze and draw a gaze circle into each image.

## Roadmap
Maybe in the future we will continue here. Right now (08/2022) the project is stopped.

## Authors and acknowledgment
Code by Mary Igbudu and Johannes Tümler.

## License
Under MIT License.
